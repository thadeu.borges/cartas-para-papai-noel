# Cartas para papai noel

CRUD de envio e leitura de cartinhas de Natal para o Papai Noel - Zappts
# NATAL REST API example application

# NATAL REST API

The REST API to the example app is described below.

## Running Locally

```sh
git clone https://gitlab.com/thadeu.borges/cartas-para-papai-noel.git
cd cartas-para-papai-noel
## renomear o arquivo .env.example para .env e ajustar a configuração com o banco de dados.
npm install
npm start
```
Your app should now be running on [localhost:3000](http://localhost:3000/).

**Acessar o SWAGGER(Documentação da API)**:

**Local - necessário alterar o "host" no swagger.json**
```
http://localhost:3000/api-docs
```

ou

**Heroku - nuvem**
```
https://noelcarta.herokuapp.com/api-docs
```

## A aplicação utiliza testes unitários com a biblioteca Mocha.js

### Teste Unitário

### Request

    npm test

### Response

    Natal API
    Successfully connected to the database
    ✓ deve recuperar todas Cartas on /cartas GET (618ms)
    ✓ deve criar a Carta on /cartas POST (402ms)
    ✓ deve atualizar o title e o content da Carta on /cartas/<id> PUT (626ms)
    ✓ Deve deletar a carta on /cartas/<id> DELETE (413ms)

    4 passing (5s)

## A aplicação utiliza autenticação via JWT, é necessário criar um login e senha
## Criar login
### Request

`POST https://noelcarta.herokuapp.com/user/signup`

    curl -i -H 'Accept: application/json' -d 'username=Nome&email=teste@gmail.com&password=123456' https://noelcarta.herokuapp.com/user/signup

### Response

    HTTP/1.1 200 OK
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 318
    ETag: W/"13e-LO8KRu7rGpvKzptF2ZAGUJP9B6o"
    Date: Sun, 27 Dec 2020 14:38:09 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5

    {"message":"Usuário registrado com sucesso!","id":"5fe89c51e0389c7d22d6b922","username":"MeuGmail","email":"meu@gmail.com","token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlODljNTFlMDM4OWM3ZDIyZDZiOTIyIn0sImlhdCI6MTYwOTA3OTg4OSwiZXhwIjoxNjA5MDg5ODg5fQ.KXTx0hMe-GwMuPlbTRqRdr6J194pk_j56-8DuiABQhM"}

## A aplicação utiliza autenticação via JWT, é necessário recuperar e passar o "TOKEN" pelo "HEADER" para consumir os métodos da API.
## Efetuar login

`POST https://noelcarta.herokuapp.com/user/login`

    curl -i -H 'Accept: application/json' -d 'email=teste@gmail.com&password=123456' https://noelcarta.herokuapp.com/user/login

## Response

    HTTP/1.1 200 OK
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 195
    ETag: W/"c3-hD+m19q6EspB2nLssQLcgu9jcjc"
    Date: Sat, 26 Dec 2020 03:37:52 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5

    {"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlNjNjZmMwNjZhNWEwMDE3NTViMTM2In0sImlhdCI6MTYwODk1Mzg3MiwiZXhwIjoxNjA4OTU3NDcyfQ.fqU7ua3Z5thvDpCQ-o8D6zsDUgvNCDcERmncx-Jd2tM"

## Recuperar todas as cartas

### Request

`GET https://noelcarta.herokuapp.com/cartas`

    curl  -i -H 'Accept: application/json' -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlNjNjZmMwNjZhNWEwMDE3NTViMTM2In0sImlhdCI6MTYwODk1Mzg3MiwiZXhwIjoxNjA4OTU3NDcyfQ.fqU7ua3Z5thvDpCQ-o8D6zsDUgvNCDcERmncx-Jd2tM' https://noelcarta.herokuapp.com/cartas

### Response

    HTTP/1.1 200 OK
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 164
    ETag: W/"1f7-Huz80coGU8kbgGAT8iX5k3gm0wc"
    Date: Thu, 24 Dec 2020 16:54:50 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5

    [{"_id":"5fe4bd60278aab2186e3f457","title":"title","content":"content","createdAt":"2020-12-24T16:10:08.747Z","updatedAt":"2020-12-24T16:25:12.994Z","__v":0}]

## Criar nova carta

### Request

`POST https://noelcarta.herokuapp.com/cartas/create`

    curl -i -H 'Accept: application/json' -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlNjNjZmMwNjZhNWEwMDE3NTViMTM2In0sImlhdCI6MTYwODk1Mzg3MiwiZXhwIjoxNjA4OTU3NDcyfQ.fqU7ua3Z5thvDpCQ-o8D6zsDUgvNCDcERmncx-Jd2tM' -d 'title=Titulo&content=conteudocarta' https://noelcarta.herokuapp.com/cartas/create

### Response

    HTTP/1.1 200 OK
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 163
    ETag: W/"a3-nU6l2wfbIDHwoBX8RsH9TAuHeIE"
    Date: Thu, 24 Dec 2020 16:56:31 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5

    {"_id":"5fe4c83fcdd23d2b91a6872d","title":"Titulo","content":"conteudocarta","createdAt":"2020-12-24T16:56:31.018Z","updatedAt":"2020-12-24T16:56:31.018Z","__v":0}

## Recuperar registro pelo ID

### Request

`GET https://noelcarta.herokuapp.com/cartas/:id`

    curl -i -H 'Accept: application/json' -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlNjNjZmMwNjZhNWEwMDE3NTViMTM2In0sImlhdCI6MTYwODk1Mzg3MiwiZXhwIjoxNjA4OTU3NDcyfQ.fqU7ua3Z5thvDpCQ-o8D6zsDUgvNCDcERmncx-Jd2tM' https://noelcarta.herokuapp.com/cartas/:id

### Response

    HTTP/1.1 200 OK
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 169
    ETag: W/"a9-LKkBxwBIXCwr0RNYNtn1Sz6NYBY"
    Date: Thu, 24 Dec 2020 17:07:45 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5

    {"_id":"5fe4bd60278aab2186e3f457","title":"title","content":"content","createdAt":"2020-12-24T16:10:08.747Z","updatedAt":"2020-12-24T16:25:12.994Z","__v":0}

## Recuperar um registro não existente

### Request

`GET https://noelcarta.herokuapp.com/cartas/:id`

    curl -i -H 'Accept: application/json' -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlNjNjZmMwNjZhNWEwMDE3NTViMTM2In0sImlhdCI6MTYwODk1Mzg3MiwiZXhwIjoxNjA4OTU3NDcyfQ.fqU7ua3Z5thvDpCQ-o8D6zsDUgvNCDcERmncx-Jd2tM' https://noelcarta.herokuapp.com/cartas/:id

### Response

    HTTP/1.1 404 Not Found
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 50
    ETag: W/"32-pktsN170/ReAFm2AWr2qTIHuBMU"
    Date: Thu, 24 Dec 2020 17:09:29 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5

    {"message":"Registro não localizado com o id id"}

## Atualizar carta
### Request

`PUT https://noelcarta.herokuapp.com/cartas/:id`

    curl -i -H 'Accept: application/json' -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlNjNjZmMwNjZhNWEwMDE3NTViMTM2In0sImlhdCI6MTYwODk1Mzg3MiwiZXhwIjoxNjA4OTU3NDcyfQ.fqU7ua3Z5thvDpCQ-o8D6zsDUgvNCDcERmncx-Jd2tM' -X PUT -d 'title=alterando&content=alterando2' https://noelcarta.herokuapp.com/cartas/:id

### Response

    HTTP/1.1 200 OK
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 163
    ETag: W/"a3-q11q3ge0RFxhUtQui+q2A9lHxfQ"
    Date: Thu, 24 Dec 2020 17:16:59 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5

    {"_id":"5fe4bd60278aab2186e3f457","title":"alterando","content":"alterando2","createdAt":"2020-12-24T16:10:08.747Z","updatedAt":"2020-12-24T17:16:59.817Z","__v":0}
## Deletar a carta pelo ID

### Request

`DELETE https://noelcarta.herokuapp.com/cartas/:id`

    curl -i -H 'Accept: application/json' -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlNjNjZmMwNjZhNWEwMDE3NTViMTM2In0sImlhdCI6MTYwODk1Mzg3MiwiZXhwIjoxNjA4OTU3NDcyfQ.fqU7ua3Z5thvDpCQ-o8D6zsDUgvNCDcERmncx-Jd2tM' -X DELETE https://noelcarta.herokuapp.com/:id

### Response

    HTTP/1.1 200 OK
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 41
    ETag: W/"29-50zPoUwNcMx0ImEvAZV4HNYM+tw"
    Date: Thu, 24 Dec 2020 17:18:57 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5


## Tentando deletar mesmo registro novamente

### Request

`DELETE https://noelcarta.herokuapp.com/cartas/:id`

    curl -i -H 'Accept: application/json' -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWZlNjNjZmMwNjZhNWEwMDE3NTViMTM2In0sImlhdCI6MTYwODk1Mzg3MiwiZXhwIjoxNjA4OTU3NDcyfQ.fqU7ua3Z5thvDpCQ-o8D6zsDUgvNCDcERmncx-Jd2tM' -X DELETE https://noelcarta.herokuapp.com/cartas/:id

### Response

    HTTP/1.1 404 Not Found
    X-Powered-By: Express
    Content-Type: application/json; charset=utf-8
    Content-Length: 69
    ETag: W/"45-Fy9Rfygp/BMcIJaHVuMzqMaEZkM"
    Date: Thu, 24 Dec 2020 17:19:22 GMT
    Connection: keep-alive
    Keep-Alive: timeout=5

    {"message":"Carta não encontrada com o id 5fe4bd60278aab2186e3f457"}
