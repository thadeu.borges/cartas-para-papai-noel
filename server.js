const express = require('express');
const bodyParser = require('body-parser');
const user = require('./app/routes/user.routes.js');
const natal = require('./app/routes/natal.routes.js');
const InitiateMongoServer = require('./config/database.config.js');

// create express app
const app = express();

// swagger
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

// parse requests of content-type - application/json
app.use(bodyParser.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

// Initiate Mongo Server
InitiateMongoServer();

// define a simple route
app.get('/', (req, res) => {
    res.json({
        "message": "Bem-vindo ao envio e leitura de cartinhas de Natal para o Papail Noel."
    });
});

/**
 * Router Middleware
 * Router - /user/*
 * Method - *
 */
app.use("/user", user);

/**
 * Router Middleware
 * Router - /cartas/*
 * Method - *
 */
app.use("/", natal);

// swagger docs
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// check Heroku port or get default
const porta = process.env.PORT || 3000;

// listen for requests
module.exports = app.listen(porta, () => {
    console.log(`Server running on port ${porta}`);
});