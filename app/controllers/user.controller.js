/**
 * Controller para cadastro e autenticacao de Usuario
 */

const User = require("../models/user.model.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {
    validationResult
} = require("express-validator");

/**
 * Criar usuario
 * @param {*} req 
 * @param {*} res 
 */
exports.signup = (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 6)
    });

    try {
        user.save((err, user) => {
            if (err) {
                res.status(500).send({
                    message: err
                });
                return;
            }
            user.save(err => {
                if (err) {
                    res.status(500).send({
                        message: err
                    });
                    return;
                }

                const payload = {
                    user: {
                        id: user.id
                    }
                };

                jwt.sign(
                    payload,
                    "randomString", {
                        expiresIn: 10000
                    },
                    (err, token) => {
                        if (err) throw err;
                        res.status(200).json({
                            message: "Usuário registrado com sucesso!",
                            id: user._id,
                            username: user.username,
                            email: user.email,
                            token: token
                        });
                    }
                );

            });
        });

    } catch (e) {
        console.error(e);
        res.status(500).json({
            message: "Ocorreu um erro no servidor."
        });
    }
};

/**
 * Autenticar login
 * @param {*} req 
 * @param {*} res 
 */
exports.login = (req, res) => {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const {
        email,
        password
    } = req.body;
    try {

        User.findOne({
                email
            })
            .exec((err, user) => {
                if (err) {
                    res.status(500).send({
                        message: err
                    });
                    return;
                }

                if (!user) {
                    return res.status(404).send({
                        message: "Usuário não existe!"
                    });
                }

                var passwordIsValid = bcrypt.compareSync(
                    password,
                    user.password
                );

                if (!passwordIsValid) {
                    return res.status(401).send({
                        message: "Senha inválida!"
                    });
                }

                const payload = {
                    user: {
                        id: user.id
                    }
                };

                jwt.sign(
                    payload,
                    "randomString", {
                        expiresIn: 3600
                    },
                    (err, token) => {
                        if (err) throw err;
                        res.status(200).json({
                            token
                        });
                    }
                );

            });

    } catch (e) {
        console.error(e);
        res.status(500).json({
            message: "Ocorreu um erro no servidor."
        });
    }
}