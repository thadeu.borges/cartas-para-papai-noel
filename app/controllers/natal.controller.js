/**
 * Controller para CRUD de cartas de natal
 */

const Natal = require('../models/natal.model.js');

/**
 * Criar nova carta
 * @param {*} req 
 * @param {*} res 
 */
exports.create = (req, res) => {

    // Validate request
    if (!req.body.content) {
        return res.status(400).send({
            message: "Carta de natal não pode ser vazia."
        });
    }

    // Create carta
    const natal = new Natal({
        title: req.body.title || "Sem titulo",
        content: req.body.content
    });

    // Save carta in the database
    natal.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocorreu algum erro ao criar a carta de natal."
            });
        });

};

/**
 * Recuperar todas cartas
 * @param {*} req 
 * @param {*} res 
 */
exports.findAll = (req, res) => {

    Natal.find()
        .then(natal => {
            res.send(natal);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocorreu algum erro recuperar as cartas."
            });
        });

};

/**
 * Recuperar carta pelo ID
 * @param {*} req 
 * @param {*} res 
 */
exports.findOne = (req, res) => {

    Natal.findById(req.params.natalId)
        .then(natal => {
            if (!natal) {
                return res.status(404).send({
                    message: "Registro não localizado com o id " + req.params.natalId
                });
            }
            res.send(natal);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Registro não localizado com o id " + req.params.natalId
                });
            }
            return res.status(500).send({
                message: "Erro consultando a carta com o id " + req.params.natalId
            });
        });

};

/**
 * Atualiza carta pelo ID
 * @param {*} req 
 * @param {*} res 
 */
exports.update = (req, res) => {

    // Validate Request
    if (!req.body.content) {
        return res.status(400).send({
            message: "Content da carta não pode ser vazia"
        });
    }

    // Find cartas and update it with the request body
    Natal.findByIdAndUpdate(req.params.natalId, {
            title: req.body.title || "",
            content: req.body.content
        }, {
            new: true
        })
        .then(natal => {
            if (!natal) {
                return res.status(404).send({
                    message: "Registro não encontrado com o id " + req.params.natalId
                });
            }
            res.send(natal);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Carta não localizada com o id " + req.params.natalId
                });
            }
            return res.status(500).send({
                message: "Erro atualizando a carta com o id " + req.params.natalId
            });
        });

};

/**
 * Delete carta pelo ID
 * @param {*} req 
 * @param {*} res 
 */
exports.delete = (req, res) => {

    Natal.findByIdAndRemove(req.params.natalId)
        .then(natal => {
            if (!natal) {
                return res.status(404).send({
                    message: "Carta não encontrada com o id " + req.params.natalId
                });
            }
            res.send({
                message: "Carta deletada com sucesso!"
            });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Carta não localizada com o id " + req.params.natalId
                });
            }
            return res.status(500).send({
                message: "Não foi possível deletar a carta com o id " + req.params.natalId
            });
        });

};