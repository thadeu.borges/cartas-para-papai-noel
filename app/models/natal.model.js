const mongoose = require('mongoose');

const NatalSchema = mongoose.Schema({
    title: String,
    content: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Natal', NatalSchema);