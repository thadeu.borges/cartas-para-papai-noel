// Endpoint testing with mocha and chai and chai-http

// Import libraries 
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mongoose = require("mongoose");

// login 
const email = 'misteryoda@gmail.com';
const password = '123456';
const username = 'yodanamefake';
const bcrypt = require("bcryptjs");

// Import server
const server = require('../../server.js');

// Import Model   
const Natal = require("../models/natal.model.js");
const User = require("../models/user.model.js");

// use chaiHttp for making the actual HTTP requests   
chai.use(chaiHttp);
describe('Natal API', function () {

    beforeEach(function (done) {
        // crio massa de dados para testes
        var newNatal = new Natal({
            title: 'Test title',
            content: 'Test content'
        });
        newNatal.save(function (err) {});

        //criou o usuario
        var newUser = new User({
            'username': username,
            'email': email,
            'password': bcrypt.hashSync(password, 6)
        });
        newUser.save(function (err) {});

        setTimeout(done, 150);
    });

    afterEach(function (done) {
        // limpo a cada each
        Natal.collection.drop().then(function () {}).catch(function () {
            console.warn('natal collection may not exists!');
        })

        // limpo o usuario
        User.collection.drop().then(function () {}).catch(function () {
            console.warn('user collection may not exists!');
        })
        
        setTimeout(done, 150);
    });

    it('deve recuperar todas Cartas on /cartas GET', function (done) {
        chai.request(server)
            .post('/user/login')
            .send({
                'email': email,
                'password': password
            })
            .end((err, res) => {
                res.body.should.have.property('token');
                var token = res.body.token;
                chai.request(server)
                    .get('/cartas')
                    .set('token', token)
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('array');
                        res.body[0].should.have.property('title');
                        res.body[0].should.have.property('content');
                        res.body[0].should.have.property('_id');
                        setTimeout(done, 150);
                    })
            })
    });

    it('deve criar a Carta on /cartas POST', function (done) {
        chai.request(server)
            .post('/user/login')
            .send({
                'email': email,
                'password': password
            })
            .end((err, res) => {
                res.body.should.have.property('token');
                var token = res.body.token;
                chai.request(server)
                    .post('/cartas/create')
                    .set('token', token)
                    .send({
                        'title': 'Test Post title',
                        'content': 'Test Post content'
                    })
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('title');
                        res.body.should.have.property('content');
                        res.body.should.have.property('_id');
                        res.body.title.should.equal('Test Post title');
                        res.body.content.should.equal('Test Post content');
                        setTimeout(done, 150);
                    });
            })
    });

    it('deve atualizar o title e o content da Carta on /cartas/<id> PUT', function (done) {
        chai.request(server)
            .post('/user/login')
            .send({
                'email': email,
                'password': password
            })
            .end((err, res) => {
                res.body.should.have.property('token');
                var token = res.body.token;
                chai.request(server)
                    .get('/cartas')
                    .set('token', token)
                    .end(function (err, res) {
                        chai.request(server)
                            .put('/cartas/' + res.body[0]._id)
                            .set('token', token)
                            .send({
                                'title': 'Test PUT title',
                                'content': 'Test PUT content'
                            })
                            .end(function (error, response) {
                                response.should.have.status(200);
                                response.should.be.json;
                                response.body.should.be.a('object');
                                response.body.should.have.property('title');
                                response.body.should.have.property('content');
                                response.body.should.have.property('_id');
                                response.body.title.should.equal('Test PUT title');
                                response.body.content.should.equal('Test PUT content');
                                setTimeout(done, 150);
                            });
                    });
            })
    });

    it('Deve deletar a carta on /cartas/<id> DELETE', function (done) {
        chai.request(server)
            .post('/user/login')
            .send({
                'email': email,
                'password': password
            })
            .end((err, res) => {
                res.body.should.have.property('token');
                var token = res.body.token;
                chai.request(server)
                    .get('/cartas')
                    .set('token', token)
                    .end(function (err, res) {
                        chai.request(server)
                            .delete('/cartas/' + res.body[0]._id)
                            .set('token', token)
                            .end(function (error, resonse) {
                                resonse.should.have.status(200);
                                resonse.body.should.have.property('message');
                                resonse.body.message.should.equal('Carta deletada com sucesso!');
                                setTimeout(done, 150);
                            });
                    })
            })
    })

});