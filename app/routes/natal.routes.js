const express = require("express");
const router = express.Router();
const controller = require('../controllers/natal.controller.js');
const auth = require("../middleware/auth.middleware.js");

// Criar nova carta
router.post('/cartas/create', auth, controller.create);

// Recuperar todas cartas
router.get('/cartas', auth, controller.findAll);

// Recuperar carta pelo ID
router.get('/cartas/:natalId', auth, controller.findOne);

// Atualizar pelo ID
router.put('/cartas/:natalId', auth, controller.update);

// Delete carta de natal
router.delete('/cartas/:natalId', auth, controller.delete);

module.exports = router;