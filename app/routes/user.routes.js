const express = require("express");
const router = express.Router();
const verifySignUp = require("../middleware/user.service.verify.js");
const controller = require('../controllers/user.controller.js');
const {
    check
} = require("express-validator");

/**
 * @method - POST
 * @param - /signup
 * @description - Cadastrar usuario
 */
router.post(
    "/signup",
    [
        verifySignUp.checkDuplicateUsernameOrEmail,
        check("username", "Informe com um nome válido.")
        .not()
        .isEmpty(),
        check("email", "Informe um email válido.").isEmail(),
        check("password", "Informe uma senha válida. Mínimo 6 caracteres.").isLength({
            min: 6
        })
    ],
    controller.signup
);

/**
 * @method - POST
 * @param - /login
 * @description - Autenticar usuario
 */
router.post(
    "/login",
    [
        check("email", "Informe um email válido.").isEmail(),
        check("password", "Informe uma senha válida. Mínimo 6 caracteres.").isLength({
            min: 6
        })
    ],
    controller.login
);

module.exports = router;