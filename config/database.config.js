require('dotenv').config();
const mongoose = require('mongoose');

const MONGOURI = process.env.MONGODB_URI;

const InitiateMongoServer = async () => {
    try {
        await mongoose.connect(MONGOURI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        });
        console.log("Successfully connected to the database");
    } catch (e) {
        console.log('Could not connect to the database. Exiting now...', e);
        throw e;
    }
};

module.exports = InitiateMongoServer;